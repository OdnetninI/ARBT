#include <iostream>

unsigned int getNewObject (unsigned int N, unsigned int M);
unsigned int insert (unsigned int proceso, unsigned int M, unsigned int* );

unsigned int procesos[100][100+1];  // Conjunto de procesos y maquinas (tiempo de ejecución), la posición M+1 se usa para almacenar si el proceso ha sido escogido o no (0 = No, 1 = Si)

int main (int argc, char* argv[]) {
	
	unsigned int numcasos = 0;
	std::cin >> numcasos;
	for (unsigned int i = 0; i < numcasos; i++) {
	  // Lectura de datos e inicialización de cada problema
	  unsigned int N = 0, M = 0; // Cantidad de Procesos y Maquinas
	  std::cin >> N >> M;
    for (unsigned int x = 0; x < N; x++) {
      for (unsigned int y = 0; y < M; y++)
        std::cin >> procesos[x][y];
      procesos[x][M] = 0;
    }
	  unsigned int tiempoCadaMaquina[M]; // Tiempo total de ejecuión
	  for (unsigned int j = 0; j < M; j++)
	    tiempoCadaMaquina[j] = 0;
	  unsigned int ordenTareas[N]; // Orden de las tares
	  unsigned int maquinaTarea[N];  // Maquina que ejecuta cada tarea
	  bool completo = false;  // Comprobar que tenemos la solución
	  
	  // Voraz:
	  unsigned int cont = 0; // Contador del numero de proceso ejecutado
	  while (!completo) {
	    // Selección
	    unsigned int proceso = getNewObject (N, M);
	    if (proceso == 0xFF) {
	      completo = true;
	      break;
	    }
	    // Factible (Siempre es factible, tenemos que insertar todos los elementos)
      ;
      // Insertar
      ordenTareas[cont] = proceso;
      unsigned int k = insert(proceso, M, tiempoCadaMaquina);
      maquinaTarea[cont] = k;
      cont++;
	  }
	  
	  // Devolver la solución
	  unsigned int tiempo = 0;
	  for (unsigned int j = 0; j < M; j++)
	    if (tiempo < tiempoCadaMaquina[j])
	      tiempo = tiempoCadaMaquina[j];
	  
	  std::cout << tiempo << std::endl;
	  for (unsigned int j = 0; j < N; j++)
	    std::cout << ordenTareas[j] +1 << " ";
	  std::cout << std::endl;
	  for (unsigned int j = 0; j < N; j++)
	    std::cout << maquinaTarea[j] +1 << " ";
	  std::cout << std::endl;
  }
  
	return 0;
}

unsigned int insert (unsigned int proceso, unsigned int M, unsigned int* tiempoCadaMaquina) {
  // Seleccionar la máquina con el tiempo de ejecución más bajo:
  unsigned int maquina = 0;
  unsigned int tiempoMaquina = 0xFFFF;
  for (unsigned int i = 0; i < M; i++) {
    if (procesos[proceso][i] < tiempoMaquina) {
      tiempoMaquina = procesos[proceso][i];
      maquina = i;
    }
  }
  tiempoCadaMaquina[maquina] += procesos[proceso][maquina];
  procesos[proceso][M] = 1;
  return maquina;
}

unsigned int getNewObject (unsigned int N, unsigned int M) {
  unsigned int mejorTiempo = 0xFFFF; // 65535 = infinito (8 tumbado)
  unsigned int proceso = 0xFF;
  
  for (unsigned int i = 0; i < N; i++) {
    unsigned int tiempoPromedio = 0;
    if (procesos[i][M] != 0) continue;
    for (unsigned int j = 0; j < M; j++) {
      tiempoPromedio += procesos[i][j];
    }
    tiempoPromedio /= M;
    if (tiempoPromedio < mejorTiempo) {
      mejorTiempo = tiempoPromedio;
      proceso = i;
    }
  }
  return proceso;
}
